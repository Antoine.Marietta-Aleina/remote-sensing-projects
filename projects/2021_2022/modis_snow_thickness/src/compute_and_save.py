# -*- coding: utf-8 -*-
"""
Created on Thu May 28 09:12:06 2020

@author: pierr
"""
"""this file contains functions to preview and save (if wanted) the products avalaible 
in the Main_Program_to_Execute"""
# =============================================================================
# External Packages ---
# Be careful to verifiy they are all installed in your Anaconda
# environnement which isn't your base(root) environnement.
# =============================================================================

import matplotlib as mpl

mpl.use("Qt5Agg")  # to use the same backend for everybody
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import rasterio as rio
import earthpy.spatial as es
import earthpy.plot as ep
from osgeo import gdal
from gdalconst import GA_ReadOnly
from mpl_toolkits.basemap import Basemap
import sys
import rasterio.plot as riop
from PIL import Image, ImageEnhance
import Clouds

# =============================================================================
# Functions
# =============================================================================


def move(pngfile):
    current_position = os.path.join(os.getcwd(), pngfile)
    future_position = os.path.join(os.path.dirname(os.getcwd()), pngfile)
    shutil.move(current_position, future_position)


def is_in_name(TAG_to_find, name, only=False):
    k = 0
    for i in range(0, len(name)):
        if name[i] == TAG_to_find[k]:
            k = k + 1
            if k == len(TAG_to_find):
                if only:
                    if i + 1 != len(name):
                        if name[i + 1] != " ":
                            return False
                        return True
                    return True
                return True
        else:
            k = 0
    return False


def compute_and_save_MOD10C1(file_name, month, day):
    """Le but de ce programme est d'à partir un fichier MOD10C1, obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus"""

    ###Viewing part###

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)
    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        return

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("fichier impossible à ouvrir " + file_name)
        return

    print(gdal.Info(in_ds))

    TAG = "Day_CMG_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0]):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_Day_CMG_Snow_Cover" + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    # Display

    gdset = gdal.Open(os.path.join(parent_file_path, ds_name))
    data = gdset.ReadAsArray()

    # Read projection parameters.
    x0, xinc, _, y0, _, yinc = gdset.GetGeoTransform()
    nx, ny = (gdset.RasterXSize, gdset.RasterYSize)

    del gdset

    # Construct the grid.  It's already in lat/lon.
    x = np.linspace(x0, x0 + xinc * nx, nx)
    y = np.linspace(y0, y0 + yinc * ny, ny)
    lon, lat = np.meshgrid(x, y)

    m = Basemap(
        projection="cyl",
        resolution="l",
        llcrnrlat=39,
        urcrnrlat=50,
        llcrnrlon=0,
        urcrnrlon=15,
    )
    m.drawcoastlines(linewidth=0.5)
    m.drawparallels(np.arange(-90.0, 120.0, 30.0), labels=[1, 0, 0, 0])
    m.drawmeridians(np.arange(-180, 180.0, 45.0), labels=[0, 0, 0, 1])
    m.drawrivers(linewidth=0.25)
    m.drawcountries(linewidth=1)
    lon_gre = 5.7167
    lat_gre = 45.1667
    x, y = m(lon_gre, lat_gre)
    m.plot(x, y, "bo", markersize=3)
    plt.text(x, y, "Grenoble")
    # m.readshapefile('Parc_AUVRA_export', 'Parc_Auvergne_Rhône-Alpes', linewidth=0.3)
    # m.readshapefile('n_enp_pnr_s_r84_export', 'Vercors')
    # m.readshapefile('PNE_coeur', 'Ecrins')

    # Bin the data as follows:
    # 0% snow
    # 1-99% snow
    # 100% snow
    # lake ice (107)
    # night (111)
    # inland water (237)
    # ocean (239)
    # cloud-obscured water (250)
    # data not mapped (253)
    # fill (255)
    lst = [
        "#00ff00",  # 0% snow
        "#888888",  # 1-99% snow
        "#ffffff",  # 100% snow
        "#ffafff",  # lake ice
        "#000000",  # night
        "#0000cc",  # inland water
        "#0000dd",  # ocean
        "#63c6ff",  # cloud-obscured water
        "#00ffcc",  # data not mapped
        "#8928dd",
    ]  # fill
    cmap = mpl.colors.ListedColormap(lst)
    bounds = [0, 100, 107, 111, 237, 239, 250, 253, 255, 256]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    # Render the image in the projected coordinate system.
    m.pcolormesh(
        lon[::2, ::2],
        lat[::2, ::2],
        data[::2, ::2],
        latlon=True,
        cmap=cmap,
        norm=norm,
    )

    date = day + "/" + month + "/2019"
    plt.title(
        "{0}\n{1}\n{2}".format("NDSI", date, "Spatial Resolution - 5km ")
    )

    color_bar = plt.colorbar(orientation="horizontal")
    color_bar.set_ticks(
        [0.5, 50, 103, 109, 180, 237.8, 242, 251.5, 254.5, 255.5]
    )
    color_bar.set_ticklabels(
        [
            "0%\nsnow",
            "1-99%\nsnow",
            "100%\nsnow",
            "lake\nice",
            "night",
            "inland\nwater",
            "ocean",
            "cloud\n-obscured\nwater",
            "data\nnot\nmapped",
            "fill",
        ]
    )
    color_bar.draw_all()
    fig = plt.gcf()

    pngfile = "5km_{0}.png".format(os.path.splitext(file_name)[0])
    fig.savefig(pngfile, dpi=1500)
    plt.close()

    move(pngfile)

    return


def compute_and_save_MOD10A1(file_name, month, day):
    """Le but de ce programme est d'à partir un fichier MOD10A1, obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    print(path_hdf_file)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()
    print("Fichier ouvert")
    # print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    gdset = gdal.Open(os.path.join(parent_file_path, ds_name))

    data = gdset.ReadAsArray()

    lst = [
        "#00ff00",  # 0% snow#ffa07a'
        "#ffc300",  # 1-20=NDSI snow0
        "#ff5733",  # 20-40=NDSI snow0
        "#c70039",  # 40-60=NDSI snow0
        "#900c3f",  # 60-80=NDSI snow0
        "#581845",  # 80-100=NDSI snow0
        "#008000",  # 200=missing data #dark green
        "#a8bb19",  # 201=no decision #acid green
        "#000000",  # 211=night
        "#1f75fe",  # 237=inland water
        "#00008b",  # 239=ocean
        "#e0ffff",  # 250=cloud
        "#f5deb3",  # 254=detector saturated
        "#a8e4a0",
    ]  # 255=fill #granny smith apple green
    cmap = mpl.colors.ListedColormap(lst)
    bounds = [0, 20, 40, 60, 80, 100, 200, 201, 211, 237, 239, 250, 254, 255]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    date = day + "/" + month + "/2019"
    title = "{0}\n{1}\n{2}".format("NDSI", date, "Spatial Resolution - 500m ")

    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=cmap, norm=norm)
    ep.draw_legend(
        im_ax=im,
        cmap=cmap,
        classes=[
            0,
            20,
            40,
            60,
            80,
            100,
            200,
            201,
            211,
            237,
            239,
            250,
            254,
            255,
        ],
        titles=[
            "NDSI : 0% snow",
            "NDSI : 1-20% snow",
            "NDSI : 20-40% snow",
            "NDSI : 40-60% snow",
            "NDSI : 60-80% snow",
            "NDSI : 80-100% snow",
            "missing data",
            "no decision",
            "night",
            "inland water",
            "ocean",
            "cloud",
            "detector saturated",
            "fill",
        ],
    )
    plt.title(title)
    ax.set_xticks([])
    ax.set_yticks([])
    try:
        manager = plt.get_current_fig_manager()
        manager.window.state("zoomed")
        pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
        fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
        plt.close()
        # move(pngfile)
    except AttributeError:
        try:
            manager = plt.get_current_fig_manager()
            manager.frame.Maximize(True)

            pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
            fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
            plt.close()
            # move(pngfile)
            plt.close()

        except AttributeError:
            try:
                manager = plt.get_current_fig_manager()
                manager.resize(*manager.window.maxsize())

                pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
                fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                plt.close()
                # move(pngfile)
                plt.close()

            except AttributeError:
                try:
                    manager = plt.get_current_fig_manager()
                    manager.window.showMaximized()

                    pngfile = "500m_{0}.png".format(
                        os.path.splitext(file_name)[0]
                    )
                    fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                    plt.close()
                    # move(pngfile)
                    plt.close()
                except:
                    print("erreur : pas de manager trouvé")
                    return


def get_data_MOD10A1(file_name, month, day):
    """
    Le but de ce programme est d'à partir un fichier MOD10A1,
    obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus
    """

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    print(path_hdf_file)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()
    print("Fichier ouvert")
    # print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    gdset = gdal.Open(os.path.join(parent_file_path, ds_name))

    data = gdset.ReadAsArray()
    return data


def compute_and_save_MOD10A1_clouds(file_name, file_name1, month, day, day1):
    """Le but de ce programme est d'à partir un fichier MOD10A1, obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    print(path_hdf_file)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()
    print("Fichier ouvert")
    # print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    gdset = gdal.Open(os.path.join(parent_file_path, ds_name))

    data1 = gdset.ReadAsArray()
    data2 = get_data_MOD10A1(file_name1, month, day1)
    data = Clouds.combinaison_nuages(data1, data2)

    lst = [
        "#00ff00",  # 0% snow#ffa07a'
        "#ffc300",  # 1-20=NDSI snow0
        "#ff5733",  # 20-40=NDSI snow0
        "#c70039",  # 40-60=NDSI snow0
        "#900c3f",  # 60-80=NDSI snow0
        "#581845",  # 80-100=NDSI snow0
        "#008000",  # 200=missing data #dark green
        "#a8bb19",  # 201=no decision #acid green
        "#000000",  # 211=night
        "#1f75fe",  # 237=inland water
        "#00008b",  # 239=ocean
        "#e0ffff",  # 250=cloud
        "#f5deb3",  # 254=detector saturated
        "#a8e4a0",
    ]  # 255=fill #granny smith apple green
    cmap = mpl.colors.ListedColormap(lst)
    bounds = [0, 20, 40, 60, 80, 100, 200, 201, 211, 237, 239, 250, 254, 255]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    date = day + "/" + month + "/2019" + " and " + day1 + "/" + month + "/2019"
    title = "{0}\n{1}\n{2}".format("NDSI", date, "Spatial Resolution - 500m ")

    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=cmap, norm=norm)
    ep.draw_legend(
        im_ax=im,
        cmap=cmap,
        classes=[
            0,
            20,
            40,
            60,
            80,
            100,
            200,
            201,
            211,
            237,
            239,
            250,
            254,
            255,
        ],
        titles=[
            "NDSI : 0% snow",
            "NDSI : 1-20% snow",
            "NDSI : 20-40% snow",
            "NDSI : 40-60% snow",
            "NDSI : 60-80% snow",
            "NDSI : 80-100% snow",
            "missing data",
            "no decision",
            "night",
            "inland water",
            "ocean",
            "cloud",
            "detector saturated",
            "fill",
        ],
    )
    plt.title(title)
    ax.set_xticks([])
    ax.set_yticks([])
    try:
        manager = plt.get_current_fig_manager()
        manager.window.state("zoomed")
        pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
        fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
        plt.close()
        # move(pngfile)
    except AttributeError:
        try:
            manager = plt.get_current_fig_manager()
            manager.frame.Maximize(True)

            pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
            fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
            plt.close()
            # move(pngfile)
            plt.close()

        except AttributeError:
            try:
                manager = plt.get_current_fig_manager()
                manager.resize(*manager.window.maxsize())

                pngfile = "500m_{0}.png".format(os.path.splitext(file_name)[0])
                fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                plt.close()
                # move(pngfile)
                plt.close()

            except AttributeError:
                try:
                    manager = plt.get_current_fig_manager()
                    manager.window.showMaximized()

                    pngfile = "500m_{0}.png".format(
                        os.path.splitext(file_name)[0]
                    )
                    fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                    plt.close()
                    # move(pngfile)
                    plt.close()
                except:
                    print("erreur : pas de manager trouvé")
                    return


def get_data_MOD10A1_cropped(file_name, month, day):

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()

    print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    # crop the file
    # get the crop contour
    crop_bound = gpd.read_file(
        os.path.join(os.getcwd(), "admin-departement.shp")
    )

    # get the same projection
    with rio.open(os.path.join(parent_file_path, ds_name)) as raster_crs:
        crop_raster_profile = raster_crs.profile
        crop_bound_utm13N = crop_bound.to_crs(crop_raster_profile["crs"])

    dss_paths = []
    dss_paths.append(os.path.join(parent_file_path, ds_name))
    with rio.open(dss_paths[0]) as src:
        single_cropped_image, single_cropped_meta = es.crop_image(
            src, crop_bound_utm13N
        )
    return single_cropped_image


def compute_and_save_MOD10A1_cropped(file_name, month, day):
    """Le but de ce programme est d'à partir un fichier MOD10A1, obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus sur une zone ressérée
    autour de l'Isère"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()

    print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    # crop the file
    # get the crop contour
    crop_bound = gpd.read_file(
        os.path.join(os.getcwd(), "admin-departement.shp")
    )

    # get the same projection
    with rio.open(os.path.join(parent_file_path, ds_name)) as raster_crs:
        crop_raster_profile = raster_crs.profile
        crop_bound_utm13N = crop_bound.to_crs(crop_raster_profile["crs"])

    dss_paths = []
    dss_paths.append(os.path.join(parent_file_path, ds_name))
    with rio.open(dss_paths[0]) as src:
        single_cropped_image, single_cropped_meta = es.crop_image(
            src, crop_bound_utm13N
        )
        # Create the extent object
    single_crop_extent = riop.plotting_extent(
        single_cropped_image[0], single_cropped_meta["transform"]
    )

    # Plotting the cropped image
    lst = [
        "#00ff00",  # 0% snow#ffa07a'
        "#ffc300",  # 1-20=NDSI snow0
        "#ff5733",  # 20-40=NDSI snow0
        "#c70039",  # 40-60=NDSI snow0
        "#900c3f",  # 60-80=NDSI snow0
        "#581845",  # 80-100=NDSI snow0
        "#008000",  # 200=missing data #dark green
        "#a8bb19",  # 201=no decision #acid green
        "#000000",  # 211=night
        "#1f75fe",  # 237=inland water
        "#00008b",  # 239=ocean
        "#e0ffff",  # 250=cloud
        "#f5deb3",  # 254=detector saturated
        "#a8e4a0",
    ]  # 255=fill #granny smith apple green
    cmap = mpl.colors.ListedColormap(lst)
    bounds = [
        1,
        20,
        40,
        60,
        80,
        100,
        200,
        201,
        211,
        237,
        239,
        250,
        254,
        255,
        256,
    ]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    fig, ax = plt.subplots()
    # crop_bound_utm13N.boundary.plot(ax=ax, color="red", zorder=10)
    date = day + "/" + month + "/2019"
    title = "{0}\n{1}\n{2}\n{3}".format(
        "NDSI",
        "Focus on Isere departement",
        date,
        "Spatial Resolution - 500m ",
    )

    fig, ax = plt.subplots()
    im = ax.imshow(single_cropped_image[0, :, :], cmap=cmap, norm=norm)

    ep.draw_legend(
        im_ax=im,
        cmap=cmap,
        classes=[
            0,
            20,
            40,
            60,
            80,
            100,
            200,
            201,
            211,
            237,
            239,
            250,
            254,
            255,
        ],
        titles=[
            "NDSI : 0% snow",
            "NDSI : 1-20% snow",
            "NDSI : 20-40% snow",
            "NDSI : 40-60% snow",
            "NDSI : 60-80% snow",
            "NDSI : 80-100% snow",
            "missing data",
            "no decision",
            "night",
            "inland water",
            "ocean",
            "cloud",
            "detector saturated",
            "fill",
        ],
    )
    plt.title(title)
    plt.close()
    ax.set_xticks([])
    ax.set_yticks([])
    try:
        manager = plt.get_current_fig_manager()
        manager.window.state("zoomed")
        pngfile = "500m_Focus_{0}.png".format(os.path.splitext(file_name)[0])
        fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
        plt.close()
        move(pngfile)
    except AttributeError:
        try:
            manager = plt.get_current_fig_manager()
            manager.frame.Maximize(True)
            pngfile = "500m_Focus_{0}.png".format(
                os.path.splitext(file_name)[0]
            )
            fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
            plt.close()
            move(pngfile)
        except AttributeError:
            try:
                manager = plt.get_current_fig_manager()
                manager.resize(*manager.window.maxsize())
                pngfile = "500m_Focus_{0}.png".format(
                    os.path.splitext(file_name)[0]
                )
                fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                plt.close()

                move(pngfile)
            except AttributeError:
                try:
                    manager = plt.get_current_fig_manager()
                    manager.window.showMaximized()
                    pngfile = "500m_Focus_{0}.png".format(
                        os.path.splitext(file_name)[0]
                    )
                    fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                    plt.close()

                    move(pngfile)
                except:
                    print("erreur : pas de manager trouvé")
                    return


def compute_and_save_MOD10A1_cropped_clouds(
    file_name, file_name1, month, day, day1
):
    """Le but de ce programme est d'à partir un fichier MOD10A1, obtenir une image de la zone voulue.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les datasets voulus sur une zone ressérée
    autour de l'Isère"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Verify the accessibility of the file
    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        sys.exit()

    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)

    # Verify it is possible to open
    if in_ds is None:
        print("Fichier impossible à ouvrir " + file_name)
        sys.exit()

    print(gdal.Info(in_ds))

    # Identify the data field.
    DATAFIELD_NAME = "NDSI_Snow_Cover"

    GRID_NAME = "MOD_Grid_Snow_500m"

    ds_name = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(
        file_name, GRID_NAME, DATAFIELD_NAME
    )

    # Open the hdf file

    TAG = "NDSI_Snow_Cover"
    target_ds = None
    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0], only=True):
            target_ds = ds[0]

    # conversion of the dataset to geotiff
    ds_name = os.path.splitext(file_name)[0] + "_" + DATAFIELD_NAME + ".tif"

    # Verify the .tif doesn't exists

    if not os.path.exists(os.path.join(parent_file_path, ds_name)):
        output = os.path.join(parent_file_path, ds_name)
        gdal.Translate(output, target_ds)

    # crop the file
    # get the crop contour
    crop_bound = gpd.read_file(
        os.path.join(os.getcwd(), "admin-departement.shp")
    )

    # get the same projection
    with rio.open(os.path.join(parent_file_path, ds_name)) as raster_crs:
        crop_raster_profile = raster_crs.profile
        crop_bound_utm13N = crop_bound.to_crs(crop_raster_profile["crs"])

    dss_paths = []
    dss_paths.append(os.path.join(parent_file_path, ds_name))
    with rio.open(dss_paths[0]) as src:
        single_cropped_image1, single_cropped_meta = es.crop_image(
            src, crop_bound_utm13N
        )
        # Create the extent object
    single_crop_extent = riop.plotting_extent(
        single_cropped_image1[0], single_cropped_meta["transform"]
    )
    data2 = get_data_MOD10A1_cropped(file_name1, month, day1)
    single_cropped_image = Clouds.combinaison_nuages(
        single_cropped_image1[0, :, :], data2[0, :, :]
    )

    # Plotting the cropped image
    lst = [
        "#00ff00",  # 0% snow#ffa07a'
        "#ffc300",  # 1-20=NDSI snow0
        "#ff5733",  # 20-40=NDSI snow0
        "#c70039",  # 40-60=NDSI snow0
        "#900c3f",  # 60-80=NDSI snow0
        "#581845",  # 80-100=NDSI snow0
        "#008000",  # 200=missing data #dark green
        "#a8bb19",  # 201=no decision #acid green
        "#000000",  # 211=night
        "#1f75fe",  # 237=inland water
        "#00008b",  # 239=ocean
        "#e0ffff",  # 250=cloud
        "#f5deb3",  # 254=detector saturated
        "#a8e4a0",
    ]  # 255=fill #granny smith apple green
    cmap = mpl.colors.ListedColormap(lst)
    bounds = [1, 20, 40, 60, 80, 100, 200, 201, 211, 237, 239, 250, 254, 255, 256]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    fig, ax = plt.subplots()
    # crop_bound_utm13N.boundary.plot(ax=ax, color="red", zorder=10)
    date = day + "/" + month + "/2019"
    title = "{0}\n{1}\n{2}\n{3}".format(
        "NDSI",
        "Focus on Isere departement",
        date,
        "Spatial Resolution - 500m ",
    )

    fig, ax = plt.subplots()
    im = ax.imshow(single_cropped_image, cmap=cmap, norm=norm)

    ep.draw_legend(
        im_ax=im,
        cmap=cmap,
        classes=[
            0,
            20,
            40,
            60,
            80,
            100,
            200,
            201,
            211,
            237,
            239,
            250,
            254,
            255,
        ],
        titles=[
            "NDSI : 0% snow",
            "NDSI : 1-20% snow",
            "NDSI : 20-40% snow",
            "NDSI : 40-60% snow",
            "NDSI : 60-80% snow",
            "NDSI : 80-100% snow",
            "missing data",
            "no decision",
            "night",
            "inland water",
            "ocean",
            "cloud",
            "detector saturated",
            "fill",
        ],
    )
    plt.title(title)
    plt.close()
    ax.set_xticks([])
    ax.set_yticks([])
    try:
        manager = plt.get_current_fig_manager()
        manager.window.state("zoomed")
        pngfile = "500m_Focus_{0}.png".format(os.path.splitext(file_name)[0])
        fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
        plt.close()
        # move(pngfile)
    except AttributeError:
        try:
            manager = plt.get_current_fig_manager()
            manager.frame.Maximize(True)
            pngfile = "500m_Focus_{0}.png".format(
                os.path.splitext(file_name)[0]
            )
            fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
            plt.close()
            # move(pngfile)
        except AttributeError:
            try:
                manager = plt.get_current_fig_manager()
                manager.resize(*manager.window.maxsize())
                pngfile = "500m_Focus_{0}.png".format(
                    os.path.splitext(file_name)[0]
                )
                fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                plt.close()

                # move(pngfile)
            except AttributeError:
                try:
                    manager = plt.get_current_fig_manager()
                    manager.window.showMaximized()
                    pngfile = "500m_Focus_{0}.png".format(
                        os.path.splitext(file_name)[0]
                    )
                    fig.savefig(pngfile, dpi=1200, bbox_inches="tight")
                    plt.close()

                    # move(pngfile)
                except:
                    print("erreur : pas de manager trouvé")
                    return


def compute_and_save_MOD09GA(file_name, month, day):
    """Le but de ce programme est d'à partir un fichier MOD09GA, obtenir une image RGB.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les les datasets qui correspondent aux 7 bandes
    puis les convertir en fichier geotiff"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Retrieve of the hdf file on the computer

    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        return
    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)
    # Verify it is possible to open
    if in_ds is None:
        print("fichier impossible à ouvrir " + file_name)
        return

    # Extraction des datasets
    dss = []
    TAG = "sur_refl_b"

    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0]):
            dss.append(ds[0])

    # Convert datasets in .tif
    i = 0
    for ds in dss:
        i += 1
        ds_name = (
            os.path.splitext(file_name)[0] + "_surf_refl_b" + str(i) + ".tif"
        )

        # Verify the .tif doesn't exists
        if not os.path.exists(os.path.join(parent_file_path, ds_name)):
            output = os.path.join(parent_file_path, ds_name)
            out_ds = gdal.Translate(output, ds)

    # Compute RGB
    modis_bands_pre_out = (
        os.path.join(parent_file_path, os.path.splitext(file_name)[0])
        + "_stack"
        + day
        + "/"
        + month
        + ".tif"
    )
    date = day + "/" + month + "/2019"

    i = 0
    dss_paths = []
    for ds in dss:
        i += 1
        ds_name = (
            os.path.splitext(file_name)[0] + "_surf_refl_b" + str(i) + ".tif"
        )
        ds_path = os.path.join(parent_file_path, ds_name)
        dss_paths.append(ds_path)

    dss_paths.sort()

    # es.stack ne peut sortir que dans le C:

    out_path = os.path.join(
        parent_file_path, "MOD09GA_stack_{0}-{1}.tif".format(day, month)
    )
    print(out_path)
    es.stack(dss_paths, out_path=out_path)

    # Open raster stack
    with rio.open(out_path) as src_modis_bands:
        modis_bands_pre = src_modis_bands.read(masked=True)
        modis_bands_bounds = src_modis_bands.bounds
        modis_bands_meta = src_modis_bands.profile
        modis_bands_crs = src_modis_bands.crs

    # Plot MODIS RGB
    title = "{0}\n{1}\n{2}".format(
        "MODIS RGB Bands - View of the Alps",
        date,
        "Spatial Resolution - 500m ",
    )

    ax = ep.plot_rgb(modis_bands_pre, rgb=[0, 3, 2], title=title, stretch=True)
    ax.set_xticks([])
    ax.set_yticks([])
    fig = plt.gcf()
    pngfile = "RGB_{0}.png".format(os.path.splitext(file_name)[0])
    fig.savefig(pngfile)
    plt.close()

    # Enhancement of the result
    im = Image.open(pngfile)
    im_bright = ImageEnhance.Brightness(im)
    im = im_bright.enhance(2)
    im_sat = ImageEnhance.Color(im)
    im = im_sat.enhance(3)
    im.save(pngfile)

    move(pngfile)


def compute_and_save_MOD09GA_cropped(file_name, month, day):
    """Le but de ce programme est d'à partir un fichier MOD09GA, obtenir une image RGB.
    Pour cela, on va récupérer le hdf, l'ouvrir, extraire les les datasets qui correspondent aux 7 bandes
    puis les convertir en fichier geotiff"""

    path_hdf_file = os.path.join(os.path.dirname(os.getcwd()), file_name)
    parent_file_path = os.path.dirname(path_hdf_file)

    # Retrieve of the hdf file on the computer

    if not os.path.isfile(path_hdf_file):
        print("Le fichier n'existe pas !\n")
        return
    # Open the hdf file
    in_ds = gdal.Open(path_hdf_file, GA_ReadOnly)
    # Verify it is possible to open
    if in_ds is None:
        print("fichier impossible à ouvrir " + file_name)
        return

    # Extraction des datasets
    dss = []
    TAG = "sur_refl_b"

    for ds in in_ds.GetSubDatasets():
        if is_in_name(TAG, ds[0]):
            dss.append(ds[0])

    # Convert datasets in .tif
    i = 0
    for ds in dss:
        i += 1
        ds_name = (
            os.path.splitext(file_name)[0] + "_surf_refl_b" + str(i) + ".tif"
        )

        # Verify the .tif doesn't exists
        if not os.path.exists(os.path.join(parent_file_path, ds_name)):
            output = os.path.join(parent_file_path, ds_name)
            out_ds = gdal.Translate(output, ds)

    # Compute RGB
    modis_bands_pre_out = (
        os.path.join(parent_file_path, os.path.splitext(file_name)[0])
        + "_stack"
        + day
        + "/"
        + month
        + ".tif"
    )

    i = 0
    dss_paths = []
    for ds in dss:
        i += 1
        ds_name = (
            os.path.splitext(file_name)[0] + "_surf_refl_b" + str(i) + ".tif"
        )
        ds_path = os.path.join(parent_file_path, ds_name)
        dss_paths.append(ds_path)

    dss_paths.sort()

    # Open the crop boundary using GeoPandas.

    crop_bound = gpd.read_file(
        os.path.join(os.getcwd(), "admin-departement.shp")
    )
    # get the same projection

    with rio.open(dss_paths[0]) as raster_crs:
        crop_raster_profile = raster_crs.profile
        crop_bound_utm13N = crop_bound.to_crs(crop_raster_profile["crs"])

    band_paths_list = es.crop_all(
        dss_paths, parent_file_path, crop_bound_utm13N, overwrite=True
    )

    cropped_array, array_raster_profile = es.stack(
        band_paths_list, nodata=-9999
    )

    with rio.open(dss_paths[0]) as src:
        single_cropped_image, single_cropped_meta = es.crop_image(
            src, crop_bound_utm13N
        )
        # Create the extent object
    single_crop_extent = riop.plotting_extent(
        single_cropped_image[0], single_cropped_meta["transform"]
    )

    # crop_extent = plotting_extent(cropped_array[0], array_raster_profile["transform"])

    # Plotting the cropped image
    # sphinx_gallery_thumbnail_number = 5

    # Plot MODIS RGB
    date = day + "/" + month + "/2019"
    title = "{0}\n{1}\n{2}".format(
        "MODIS RGB Bands - Focus on Isere departement",
        date,
        "Spatial Resolution - 500m ",
    )
    fig, ax = plt.subplots(figsize=(12, 6))
    crop_bound_utm13N.boundary.plot(ax=ax, color="red", zorder=10)
    ax = ep.plot_rgb(
        cropped_array,
        rgb=[0, 3, 2],
        ax=ax,
        stretch=True,
        title=title,
        extent=single_crop_extent,
    )
    ax.set_xticks([])
    ax.set_yticks([])

    # enregistrement
    fig = plt.gcf()
    pngfile = "RGB_Focus_{0}.png".format(os.path.splitext(file_name)[0])
    fig.savefig(pngfile)
    plt.close()

    # Enhancement of the result
    im = Image.open(pngfile)
    im_bright = ImageEnhance.Brightness(im)
    im = im_bright.enhance(2)
    im_sat = ImageEnhance.Color(im)
    im = im_sat.enhance(3)
    im.save(pngfile)

    move(pngfile)


# compute_and_save_MOD09GA("MOD09GA.A2019046.h18v04.006.2019050234433.hdf",'02','15')
